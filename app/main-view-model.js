const Observable = require("tns-core-modules/data/observable").Observable;
const topmost = require("tns-core-modules/ui/frame").topmost;

function createViewModel() {
    const viewModel = new Observable();
    viewModel.username="a@gmail.com";
    viewModel.password="123";
    viewModel.firstname="NativeScript";
    viewModel.lastname="Javascript";
    viewModel.city="Mumbai";
    viewModel.state="Maharashtra";
    viewModel.country="India";
    
    // let str = document.getElementById("user");

    viewModel.onTap = () => {
    
        if(viewModel.username.trim() === "" || viewModel.password.trim() === "" || viewModel.firstname.trim() === "" ||
                       viewModel.state.trim() === "" || viewModel.country.trim() === "" ){
                    alert("Please provide full details");
                    return;
        }else{
            topmost().navigate({
                moduleName: "./sample/sample",
                clearHistory: true
            });
            alert("Registered Successfull")
        }  
    };

  

    return viewModel;
}

exports.createViewModel = createViewModel;
