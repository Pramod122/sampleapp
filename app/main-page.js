const dialogsModule = require("ui/dialogs");
const createViewModel = require("./main-view-model").createViewModel;

function onNavigatingTo(args) {
    const page = args.object;
     page.bindingContext = createViewModel();
}

test()

exports.onNavigatingTo = onNavigatingTo;



// exports.login = function () {
//     dialogsModule.login({
//       title: "Your title",
//       message: "Your message",
//       okButtonText: "Your button text",
//       cancelButtonText: "Cancel text",
//       neutralButtonText: "Neutral text",
//       userName: "User name label text",
//       password: "Password label text"
//     }).then((result) => {
//       // The result property is true if the dialog is closed with the OK button, false if closed with the Cancel button or undefined if closed with a neutral button.
//       console.log("Dialog result: " + result.result);
//       console.log("Username: " + result.userName);
//       console.log("Password: " + result.password);
//     });
// };


// exports.alert = function () {
//     dialogsModule.alert({
//       title: "Your title",
//       message: "Your message",
//       okButtonText: "Your button text"
//     }).then(() => {
//       console.log("The user closed the alert.");
//     });
// };

// exports.confirm = function () {
//     dialogsModule.confirm({
//       title: "Your title",
//       message: "Your message",
//       okButtonText: "Your button text",
//       cancelButtonText: "Cancel text",
//       neutralButtonText: "Neutral text"
//     }).then((result) => {
//       // The result property is true if the dialog is closed with the OK button, false if closed with the Cancel button or undefined if closed with a neutral button.
//       console.log("Dialog result: " + result);
//     });
// };

// exports.prompt = function () {
//     dialogsModule.prompt({
//       title: "Your title",
//       message: "Your message",
//       okButtonText: "Your button text",
//       cancelButtonText: "Cancel text",
//       neutralButtonText: "Neutral text",
//       defaultText: "Default text",
//       inputType: dialogsModule.inputType.email
//     }).then((result) => {
//       // The result property is true if the dialog is closed with the OK button, false if closed with the Cancel button or undefined if closed with a neutral button.
//       console.log("Dialog result: " + result.result);
//       console.log("Text: " + result.text);
//     })
// };


// exports.action = function () {
//     dialogsModule.action({
//       message: "Your message",
//       cancelButtonText: "Cancel text",
//       actions: ["Option1", "Option2"]
//     }).then((result) => {
//       if (result == "Option1") {
//         console.log("The user selected option 1.");
//       } else {
//         console.log("The user selected option 2.");
//       }
//     });
// };

// exports.buttonPressed = () => {
//     alert("Button Pressed!!!")
// }
