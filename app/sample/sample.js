var dialogs = require("tns-core-modules/ui/dialogs");
const SampleViewModel = require("./sample-view-model");
const sampleviewmodel = new SampleViewModel();


exports.sampleFunction = function(args) {
  const page = args.object;
  page.bindingContext = sampleviewmodel;
}


exports.login = function () {
dialogs.prompt({
  title: "Your title",
  message: "Your message",
  okButtonText: "Your button text",
  cancelButtonText: "Cancel text",
  neutralButtonText: "Neutral text",
  defaultText: "Default text",
  inputType: dialogs.inputType.password
}).then(function (r) {
  console.log("Dialog result: " + r.result + ", text: " + r.text);
});


};
